SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<John Mackin>
-- Create date: <20-May-2015>
-- Description:	<Maintains - initially - Off set values for DIM DATE>
-- =============================================
-- DROP PROCEDURE MAINTAIN_DIMDATE_DAILY;

CREATE PROCEDURE [dbo].[MAINTAIN_DIMDATE_DAILY]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET DATEFIRST 1;  -- Later comment: Specifying SET DATEFIRST has no effect on DATEDIFF. DATEDIFF always uses Sunday as the first day of the week to ensure the function is deterministic.
	

   UPDATE DimDate
    SET [OffsetYear] = DATEDIFF (YEAR , GETDATE(), [Date]),
    [OffsetMonth] = DATEDIFF (MONTH , GETDATE(),[Date] ),
	[OffsetWeek] = DATEDIFF (WEEK, GETDATE(), [Date]),
    [OffsetDay] = DATEDIFF (DAY , GETDATE(), [Date]) ;
--
	UPDATE DIMDATE -- Added so that OffSet weeks are Monday (1st Day) to Sunday (7th) (UK), not Sunday to Saturday (US) - See comment above. 
	SET OffSetWeek = OffSetWeek -1 
	WHERE DayOfWeek = 'Sunday';
	--
END
GO
