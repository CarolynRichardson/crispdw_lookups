EXEC sp_addrolemember N'db_datareader', N'BlackburnP'
GO
EXEC sp_addrolemember N'db_datareader', N'ChoiP'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_A'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_B'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_C'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_D'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_E'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_Migration_A'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_Migration_B'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_Migration_C'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_Migration_D'
GO
EXEC sp_addrolemember N'db_datareader', N'Crisp_Migration_E'
GO
EXEC sp_addrolemember N'db_datareader', N'Dalstont'
GO
EXEC sp_addrolemember N'db_datareader', N'ETL_Admin'
GO
EXEC sp_addrolemember N'db_datareader', N'FahyL'
GO
EXEC sp_addrolemember N'db_datareader', N'GavanM'
GO
EXEC sp_addrolemember N'db_datareader', N'GudimetlaR'
GO
EXEC sp_addrolemember N'db_datareader', N'hagertyj'
GO
EXEC sp_addrolemember N'db_datareader', N'HareB'
GO
EXEC sp_addrolemember N'db_datareader', N'Jonesa'
GO
EXEC sp_addrolemember N'db_datareader', N'jonesc'
GO
EXEC sp_addrolemember N'db_datareader', N'KellyS'
GO
EXEC sp_addrolemember N'db_datareader', N'LadJ'
GO
EXEC sp_addrolemember N'db_datareader', N'LyonJ'
GO
EXEC sp_addrolemember N'db_datareader', N'mackinj'
GO
EXEC sp_addrolemember N'db_datareader', N'MacphersonB'
GO
EXEC sp_addrolemember N'db_datareader', N'MaddenP'
GO
EXEC sp_addrolemember N'db_datareader', N'Masong'
GO
EXEC sp_addrolemember N'db_datareader', N'MDISDEV\CRISPExecution'
GO
EXEC sp_addrolemember N'db_datareader', N'MDISDEV\T-GBBHSRDB03_SQL_DDLAdmin'
GO
EXEC sp_addrolemember N'db_datareader', N'MDISDEV\T-GBBHSRDB03_SQL_ReportView'
GO
EXEC sp_addrolemember N'db_datareader', N'ObrienI'
GO
EXEC sp_addrolemember N'db_datareader', N'ObrienS'
GO
EXEC sp_addrolemember N'db_datareader', N'OstickS'
GO
EXEC sp_addrolemember N'db_datareader', N'PodmoreC'
GO
EXEC sp_addrolemember N'db_datareader', N'RaeC'
GO
EXEC sp_addrolemember N'db_datareader', N'rawsonc'
GO
EXEC sp_addrolemember N'db_datareader', N'Report_User'
GO
EXEC sp_addrolemember N'db_datareader', N'sodamolaj'
GO
EXEC sp_addrolemember N'db_datareader', N'SQLReports'
GO
EXEC sp_addrolemember N'db_datareader', N'SwanickM'
GO
EXEC sp_addrolemember N'db_datareader', N'WilliamsH'
GO
EXEC sp_addrolemember N'db_datareader', N'WilliamsS'
GO
