EXEC sp_addrolemember N'db_datawriter', N'BlackburnP'
GO
EXEC sp_addrolemember N'db_datawriter', N'ChoiP'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_A'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_B'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_C'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_D'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_E'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_Migration_A'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_Migration_B'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_Migration_C'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_Migration_D'
GO
EXEC sp_addrolemember N'db_datawriter', N'Crisp_Migration_E'
GO
EXEC sp_addrolemember N'db_datawriter', N'Dalstont'
GO
EXEC sp_addrolemember N'db_datawriter', N'ETL_Admin'
GO
EXEC sp_addrolemember N'db_datawriter', N'FahyL'
GO
EXEC sp_addrolemember N'db_datawriter', N'GavanM'
GO
EXEC sp_addrolemember N'db_datawriter', N'GudimetlaR'
GO
EXEC sp_addrolemember N'db_datawriter', N'hagertyj'
GO
EXEC sp_addrolemember N'db_datawriter', N'HareB'
GO
EXEC sp_addrolemember N'db_datawriter', N'Jonesa'
GO
EXEC sp_addrolemember N'db_datawriter', N'jonesc'
GO
EXEC sp_addrolemember N'db_datawriter', N'KellyS'
GO
EXEC sp_addrolemember N'db_datawriter', N'LadJ'
GO
EXEC sp_addrolemember N'db_datawriter', N'LyonJ'
GO
EXEC sp_addrolemember N'db_datawriter', N'mackinj'
GO
EXEC sp_addrolemember N'db_datawriter', N'MacphersonB'
GO
EXEC sp_addrolemember N'db_datawriter', N'MaddenP'
GO
EXEC sp_addrolemember N'db_datawriter', N'Masong'
GO
EXEC sp_addrolemember N'db_datawriter', N'MDISDEV\CRISPExecution'
GO
EXEC sp_addrolemember N'db_datawriter', N'MDISDEV\T-GBBHSRDB03_SQL_DDLAdmin'
GO
EXEC sp_addrolemember N'db_datawriter', N'ObrienI'
GO
EXEC sp_addrolemember N'db_datawriter', N'ObrienS'
GO
EXEC sp_addrolemember N'db_datawriter', N'OstickS'
GO
EXEC sp_addrolemember N'db_datawriter', N'PodmoreC'
GO
EXEC sp_addrolemember N'db_datawriter', N'RaeC'
GO
EXEC sp_addrolemember N'db_datawriter', N'rawsonc'
GO
EXEC sp_addrolemember N'db_datawriter', N'sodamolaj'
GO
EXEC sp_addrolemember N'db_datawriter', N'SQLReports'
GO
EXEC sp_addrolemember N'db_datawriter', N'SwanickM'
GO
EXEC sp_addrolemember N'db_datawriter', N'WilliamsH'
GO
EXEC sp_addrolemember N'db_datawriter', N'WilliamsS'
GO
