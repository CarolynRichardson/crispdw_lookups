IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'rawsonc')
CREATE LOGIN [rawsonc] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [rawsonc] FOR LOGIN [rawsonc]
GO
