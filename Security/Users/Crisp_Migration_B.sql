IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_Migration_B')
CREATE LOGIN [Crisp_Migration_B] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_Migration_B] FOR LOGIN [Crisp_Migration_B]
GO
