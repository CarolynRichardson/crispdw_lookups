IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'WilliamsH')
CREATE LOGIN [WilliamsH] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [WilliamsH] FOR LOGIN [WilliamsH]
GO
GRANT SHOWPLAN TO [WilliamsH]
