IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Dalstont')
CREATE LOGIN [Dalstont] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Dalstont] FOR LOGIN [Dalstont]
GO
GRANT SHOWPLAN TO [Dalstont]
