IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MacphersonB')
CREATE LOGIN [MacphersonB] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [MacphersonB] FOR LOGIN [MacphersonB]
GO
GRANT SHOWPLAN TO [MacphersonB]
