IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'mackinj')
CREATE LOGIN [mackinj] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [mackinj] FOR LOGIN [mackinj]
GO
GRANT SHOWPLAN TO [mackinj]
