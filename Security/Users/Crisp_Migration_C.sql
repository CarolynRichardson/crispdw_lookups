IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_Migration_C')
CREATE LOGIN [Crisp_Migration_C] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_Migration_C] FOR LOGIN [Crisp_Migration_C]
GO
