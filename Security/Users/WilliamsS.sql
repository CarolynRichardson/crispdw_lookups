IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'WilliamsS')
CREATE LOGIN [WilliamsS] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [WilliamsS] FOR LOGIN [WilliamsS]
GO
GRANT SHOWPLAN TO [WilliamsS]
