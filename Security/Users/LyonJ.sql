IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LyonJ')
CREATE LOGIN [LyonJ] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LyonJ] FOR LOGIN [LyonJ]
GO
GRANT SHOWPLAN TO [LyonJ]
