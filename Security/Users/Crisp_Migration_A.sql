IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_Migration_A')
CREATE LOGIN [Crisp_Migration_A] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_Migration_A] FOR LOGIN [Crisp_Migration_A]
GO
