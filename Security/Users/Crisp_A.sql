IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_A')
CREATE LOGIN [Crisp_A] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_A] FOR LOGIN [Crisp_A]
GO
