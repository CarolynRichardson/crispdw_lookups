IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'PodmoreC')
CREATE LOGIN [PodmoreC] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [PodmoreC] FOR LOGIN [PodmoreC]
GO
GRANT SHOWPLAN TO [PodmoreC]
