IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Jonesa')
CREATE LOGIN [Jonesa] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Jonesa] FOR LOGIN [Jonesa]
GO
GRANT SHOWPLAN TO [Jonesa]
