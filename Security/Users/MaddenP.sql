IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MaddenP')
CREATE LOGIN [MaddenP] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [MaddenP] FOR LOGIN [MaddenP]
GO
GRANT SHOWPLAN TO [MaddenP]
