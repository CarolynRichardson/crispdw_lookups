IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'GudimetlaR')
CREATE LOGIN [GudimetlaR] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [GudimetlaR] FOR LOGIN [GudimetlaR]
GO
