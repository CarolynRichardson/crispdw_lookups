IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_Migration_D')
CREATE LOGIN [Crisp_Migration_D] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_Migration_D] FOR LOGIN [Crisp_Migration_D]
GO
