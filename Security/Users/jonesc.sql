IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'jonesc')
CREATE LOGIN [jonesc] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [jonesc] FOR LOGIN [jonesc]
GO
GRANT SHOWPLAN TO [jonesc]
