IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ObrienS')
CREATE LOGIN [ObrienS] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [ObrienS] FOR LOGIN [ObrienS]
GO
GRANT SHOWPLAN TO [ObrienS]
