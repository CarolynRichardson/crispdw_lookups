IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'OstickS')
CREATE LOGIN [OstickS] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [OstickS] FOR LOGIN [OstickS]
GO
GRANT SHOWPLAN TO [OstickS]
