IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SwanickM')
CREATE LOGIN [SwanickM] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [SwanickM] FOR LOGIN [SwanickM]
GO
GRANT SHOWPLAN TO [SwanickM]
