IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FahyL')
CREATE LOGIN [FahyL] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [FahyL] FOR LOGIN [FahyL]
GO
GRANT SHOWPLAN TO [FahyL]
