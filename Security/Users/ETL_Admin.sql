IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ETL_Admin')
CREATE LOGIN [ETL_Admin] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [ETL_Admin] FOR LOGIN [ETL_Admin]
GO
