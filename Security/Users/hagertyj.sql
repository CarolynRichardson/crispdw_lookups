IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'hagertyj')
CREATE LOGIN [hagertyj] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [hagertyj] FOR LOGIN [hagertyj]
GO
GRANT SHOWPLAN TO [hagertyj]
