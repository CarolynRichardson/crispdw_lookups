IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Masong')
CREATE LOGIN [Masong] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Masong] FOR LOGIN [Masong]
GO
GRANT SHOWPLAN TO [Masong]
