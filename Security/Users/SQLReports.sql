IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SQLReports')
CREATE LOGIN [SQLReports] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [SQLReports] FOR LOGIN [SQLReports]
GO
