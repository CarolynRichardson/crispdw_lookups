IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ChoiP')
CREATE LOGIN [ChoiP] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [ChoiP] FOR LOGIN [ChoiP]
GO
