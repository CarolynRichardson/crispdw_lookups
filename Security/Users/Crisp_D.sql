IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_D')
CREATE LOGIN [Crisp_D] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_D] FOR LOGIN [Crisp_D]
GO
