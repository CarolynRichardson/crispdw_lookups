IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'HareB')
CREATE LOGIN [HareB] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [HareB] FOR LOGIN [HareB]
GO
GRANT SHOWPLAN TO [HareB]
