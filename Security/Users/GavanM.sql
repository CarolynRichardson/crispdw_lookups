IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'GavanM')
CREATE LOGIN [GavanM] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [GavanM] FOR LOGIN [GavanM]
GO
GRANT SHOWPLAN TO [GavanM]
