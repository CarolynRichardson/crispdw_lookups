IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KellyS')
CREATE LOGIN [KellyS] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [KellyS] FOR LOGIN [KellyS]
GO
GRANT SHOWPLAN TO [KellyS]
