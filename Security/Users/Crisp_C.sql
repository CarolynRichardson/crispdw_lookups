IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_C')
CREATE LOGIN [Crisp_C] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_C] FOR LOGIN [Crisp_C]
GO
