IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_Migration_E')
CREATE LOGIN [Crisp_Migration_E] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_Migration_E] FOR LOGIN [Crisp_Migration_E]
GO
