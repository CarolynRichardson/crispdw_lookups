IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'sodamolaj')
CREATE LOGIN [sodamolaj] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [sodamolaj] FOR LOGIN [sodamolaj]
GO
GRANT SHOWPLAN TO [sodamolaj]
