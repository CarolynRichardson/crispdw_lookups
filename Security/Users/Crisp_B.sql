IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_B')
CREATE LOGIN [Crisp_B] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_B] FOR LOGIN [Crisp_B]
GO
