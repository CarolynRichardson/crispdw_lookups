IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Crisp_E')
CREATE LOGIN [Crisp_E] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Crisp_E] FOR LOGIN [Crisp_E]
GO
