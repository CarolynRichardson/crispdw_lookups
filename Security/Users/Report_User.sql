IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Report_User')
CREATE LOGIN [Report_User] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Report_User] FOR LOGIN [Report_User]
GO
GRANT SHOWPLAN TO [Report_User]
