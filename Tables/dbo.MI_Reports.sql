CREATE TABLE [dbo].[MI_Reports]
(
[Rpt_ID] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[Rpt_DisplayName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Rpt_IT_Descr] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Rpt_User_Descr] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[Rpt_Owner] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Rpt_Owner_Contact] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Rpt_Type] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Rpt_GDPR_Advice] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Rpt_Restrict_Access] [int] NULL CONSTRAINT [DF__MI_Report__Restr__276EDEB3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MI_Reports] ADD CONSTRAINT [PK__MI_Repor__4D5FD39B599A4089] PRIMARY KEY CLUSTERED  ([Rpt_ID]) ON [PRIMARY]
GO
