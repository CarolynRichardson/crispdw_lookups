SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[CalculateNumberOFWorkDays] (@StartDate DATE, @EndDate DATE)
RETURNS INT
AS
BEGIN
     
     SET @StartDate = DATEADD(dd, DATEDIFF(dd, 0, @StartDate), 0) 
     SET @EndDate = DATEADD(dd, DATEDIFF(dd, 0, @EndDate), 0) 
     --     
     DECLARE @WORKDAYS INT = 0
	 DECLARE @Holidays INT = 0
	 --
     SELECT @WORKDAYS = (DATEDIFF(dd, @StartDate, @EndDate))  
	               -(DATEDIFF(wk, @StartDate, @EndDate) * 2)
   		       -(CASE WHEN DATENAME(dw, @StartDate) = 'Sunday' THEN 1 ELSE 0 END)
		       -(CASE WHEN DATENAME(dw, @EndDate) = 'Saturday' THEN 1 ELSE 0 END);
     --
	 -- At this point deduct number of bank holidays between the 2 dates
	 -- by referring to the DIM_DATE table. (JM)
	 --
	 SELECT @Holidays =
	   (SELECT COUNT(*)
	   FROM CRISPDW_LookUps.dbo.DimDate
	   WHERE Date BETWEEN @StartDate AND @EndDate
	   AND HolidayText IS NOT NULL
	   AND DayOfWeek NOT IN ('Saturday', 'Sunday'));
     --
	 SELECT @WORKDAYS = @WORKDAYS - @Holidays;
	 --
	 -- Remove the 'current' / first day unless the value is 1. -- This could be a flaw!
	 --
	-- This needs to be discussed!
--	 SELECT @workdays =
--	    CASE WHEN @WORKDAYS > 1 THEN @WORKDAYS -1
--	 	       ELSE 0
 --     END;

 -- Does not include the first or last day. just DAYS BETWEEN!

			   
  
     RETURN @WORKDAYS
END

GO
GRANT EXECUTE ON  [dbo].[CalculateNumberOFWorkDays] TO [Jonesa]
GO
GRANT ALTER ON  [dbo].[CalculateNumberOFWorkDays] TO [Jonesa]
GO
GRANT TAKE OWNERSHIP ON  [dbo].[CalculateNumberOFWorkDays] TO [Jonesa]
GO
GRANT EXECUTE ON  [dbo].[CalculateNumberOFWorkDays] TO [Report_User]
GO
GRANT ALTER ON  [dbo].[CalculateNumberOFWorkDays] TO [Report_User]
GO
GRANT TAKE OWNERSHIP ON  [dbo].[CalculateNumberOFWorkDays] TO [Report_User]
GO
